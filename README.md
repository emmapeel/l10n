Localization for the Tor Project

This section organizes the Localization work for the
Tor Project.

If you want to translate Tor apps to your language,
please read:
https://community.torproject.org/localization/becoming-tor-translator/ 
and our [wiki](https://gitlab.torproject.org/tpo/community/l10n/-/wikis/Localization-for-translators)

If you want your application to be translated,
please read: [this documentation section](https://gitlab.torproject.org/tpo/community/l10n/-/wikis/Localization-for-developers)

The more general localization issues are on this project's bugtracker, but there are also l10n tickets in other projects.
[See all localization issues](https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&label_name[]=Localization)

There are some l10n QA scripts on the `bin/` directory of this repository, to be used by gitlabCI.

The translations for our software are hosted in this other repository: https://gitlab.torproject.org/tpo/translation/
