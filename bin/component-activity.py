#!/usr/bin/env python3
# this script is based/copied from
# Micah Lee's script for OnionShare
# translations at
# https://github.com/onionshare/onionshare/blob/main/docs/check-weblate.py
# modified by emmapeel for the Tor Project - 2023
import sys
import asyncio
import configparser
import os.path
import json
from operator import itemgetter
from itertools import groupby
import httpx
import markdown
import datetime
from markdown.extensions.toc import TocExtension




# this config is mantained 'by hand' and gives information about released
# languages and other l10n related matters:
resources_config = configparser.ConfigParser()
resources_config.read("./ci-templates/stats/resources-config.ini")


api_token = None
languages = {}
ignored_languages = "am kab pbb"
ignored_users = ['anonymous', 'mt:weblate', 'mt:weblate-translation-memory' ]
#ignored_users = ['anonymous', 'emmapeel', None, 'mt:weblate', 'mt:weblate-translation-memory' ]
component_translations = {}
component_changes = {}
component_categories = {}
base = datetime.date.today() - datetime.timedelta(days=1)

# You can configure some things here
changes_from = '2024-01-01'
#change the range number to have more or less days of contributors data
contributors_data_range = 25
missing_translations = {}
help_needed = {}

# here some utilities...

date_list = [base - datetime.timedelta(days=x) for x in range(contributors_data_range)]

dt = datetime.datetime.utcnow()
date_built = dt.strftime("%A, %d %B, %Y %I:%M%p")
nice_date_built = dt.strftime("%A, %d %B, %Y")


nice_changes_from = datetime.datetime.strptime(changes_from, '%Y-%m-%d').strftime('%A, %d %B, %Y')
def make_html(markdown_file, html_path):
    # util function to create a file in /public/
    # with the markdown file
    markdown.markdownFromFile(
        input=markdown_file,
        extensions=[
            "tables",
            "md_in_html",
            TocExtension(toc_depth="2-3"),
            ],
            output='stats/tmp.html',
            )
    filenames = [
        "./ci-templates/admin-stats-header.html",
        "./stats/tmp.html",
        "./ci-templates/stats-footer.html",
    ]
    with open(html_path, "w") as outfile:
        for fname in filenames:
            with open(fname) as infile:
                for line in infile:
                    outfile.write(line)


def write_logs(markdown_file_path, newstring):
    if not os.path.exists(markdown_file_path):
        print(f'Creating { markdown_file_path }')
        with open(markdown_file_path, "w") as log_file:
            log_file.write(newstring)
    else:
        with open(markdown_file_path, "a") as log_file:
            log_file.write(newstring)

def write_comp_source_stats(component, newstring):
    # shortcut to write to the component admin stats file
    markdown_path = f'stats/c-adm-{ component }.md'
    write_logs(markdown_path, newstring)

def comp_link(component_slug):
    # from a component slug, make a nice link to the
    # component page, using the name instead of the slug
    mytext = comp_nice_name(component_slug)
    myurl =  f'c-adm-{ component_slug.replace("252F","25252F")}.html'
    #url =  component_translations[component_slug]['web_url']
    built_md = f'[{ comp_nice_name(component_slug) }]({ myurl })'
    return built_md


def table_headings(headings):
    # give a list of strings, and the are the headings of your table
    myheader = '\n|'
    for heading in headings:
        myheader += f' { heading } |'
    myheader += '\n|'
    for heading in headings:
        myheader += ' --- |'
    myheader += '\n'
    return myheader

def table_line(myvalues):
    myline = '|'
    for onevalue in myvalues:
        myline += f' { onevalue } |'
    myline += '\n'
    return myline

def page_header(onetitle):
    return f'\n# { onetitle }\n*built on { date_built } UTC*\n\n[TOC]\n\n'

def comp_nice_name(component_slug):
    # To quickly get the component nice name for tables and such
    if component_slug not in component_translations.keys():
        print(f'bad luck with { component_slug }!')
        return component_slug
    else:
        return component_translations[component_slug]['name']

def change_src_link(actionorchange):
    #util to generate an api search for a type of change for the day of the action
    mydate = actionorchange['date']
    actionnr = actionorchange['actionnr']
    url = f"[{ actionorchange['action'] }]("
    url += f"https://hosted.weblate.org/search/tor/"
    url += f"{ actionorchange['component'].replace('%252F', '/') }/?q="
    url += f"+added%3A{ mydate }+AND+language%3Aen)"

    #tor/tor-browser/tb-base-browser/?q=+added%3A2024-05-16+AND+language%3Aen&sort_by=-priority%2Cposition&checksum=
    return url

def write_comp_source_stats(component, newstring):
    # shortcut to write to the component admin stats file
    markdown_path = f'stats/c-adm-{ component }.md'
    write_logs(markdown_path, newstring)



async def api(path):
    global languages
    url = f"https://hosted.weblate.org{path}"
    # print(url)
    cached_file = f'./stats/{"-".join( path.split("/")[2:]).replace("api/","").replace("-tor-","-").replace("&action=","=").replace("components-","comp-") }.json'
    # we try to reuse the json if we already downloaded it
    if os.path.exists(cached_file):
        with open(cached_file) as f:
            data = json.load(f)
            return data
    else:

        # Wait a bit before each API call, to avoid hammering the server and
        # getting temporarily blocked
        await asyncio.sleep(1)
        async with httpx.AsyncClient() as client:
            r = await client.get(
                url,
                headers={"Authorization": f"Token {api_token}"},
                timeout=60,
            )

        if r.status_code == 200:
            print(f"GET {url}")
            with open(cached_file, "w") as output_file:
                json.dump(
                    r.json(),
                    output_file,
                    sort_keys=True,
                    ensure_ascii=False,
                    indent=4,
                )

            return r.json()
        else:
            print(f"GET {url} | error {r.status_code}")
            return None


async def add_stats_to_component(obj, component):
    # process a json result object line and
    # adds the translation percentage to the results
    global component_translations
    for res in obj:
        if res["code"] != "en":

            if not res["code"] in component_translations[component]:
                component_translations[component][res["code"]] = {}
            component_translations[component][res["code"]]["released"] = False
            try:
                component_translations[component][res["code"]][
                    "last_change"
                ] = datetime.strptime(
                    res["last_change"], "%Y-%m-%dT%H:%M:%S.%f%z"
                )
            except AttributeError:
                component_translations[component][res["code"]][
                    "last_change"
                ] = None
            for newvalue in [
                "total",
                "total_words",
                "translated_words",
                "translated_percent",
                "fuzzy",
                "fuzzy_percent",
                "approved_words",
                "recent_changes",
                "failing",
                "suggestions",
                "comments",
                "translate_url",
            ]:
                component_translations[component][res["code"]][newvalue] = res[
                    newvalue
                ]


async def get_translations(component):
    global component_translations
    if component not in resources_config.keys():
        if component not in resources_config.values():
        # lets remind ourselvs to add the released languages to the config,
        # to prioritise translations of stuff already in production
            print(
            f'please add { component } to the config file at "./ci-templates/stats/resources-config.ini'
            )

    try:
        obj = await api(f"/api/components/tor/{component}/statistics/")
        await add_stats_to_component(obj["results"], component)

        while obj["next"] != None:
            newpage = obj["next"].split("/")[-1]
            obj = await api(
                f"/api/components/tor/{component}/statistics/{newpage}"
            )
            await add_stats_to_component(obj["results"], component)
    except TypeError:
        pass



async def get_one_change_for_a_component(component_name, change):
    global component_changes
    # here we gather the stats about translations, translators activity
    # some repository operations and other actions are not important are ignored.
    # you can see what they are in
    # https://hosted.weblate.org/changes/browse/tor/support-portal/?action=5

    counted_actions = {}
    ignored_actions = [17, 18, 21, 25, 26, 29, 32, 40, 47, 53, 56, 59, 60, 61, 62, 64 ]
    #ignored_actions = [17, 18, 25, 26, 29, 53, 56, 59]
    if change["action"] in ignored_actions:
        print('ignored:')
        print(change["action"])
        pass
    if change["action"] not in ignored_actions:
        if change["action_name"] not in counted_actions.keys():
            counted_actions[change["action_name"]] = 0
        counted_actions[change["action_name"]] += 1

        change["actionnr"] = change["action"]
        change["action"] = change["action_name"]
        if change['translation'] != None:
            change['language'] = change['translation'].split('/')[7]

        if change["user"] != None:
            user = change["user"].split("/")[-2]
            ignored_users = ['anonymous', 'emmapeel', None]
            if user in ignored_users:
                print('this change used to be ignored!')
    if change['id'] not in component_changes:
        component_changes[change['id']] = change

async def get_source_info(thisunit, inform, infodefault):
    # thisunit is a json url for a string (source or target):
    # inform is the key we look for
    # infodefault is the default value if we dont find anything
    unit_id = thisunit.split("/")[5]
    obj = await api(
            f"/api/units/{ unit_id }/?format=json"
        )
    if obj != None:
        # lets make sure the object is a source string
        if obj['source_unit'] == thisunit:
            if inform in obj.keys():
                return obj[inform]
            else:
                return infodefault
        else:
            unit_id = obj['source_unit'].split("/")[5]
            obj = await api(
                f"/api/units/{ unit_id }/?format=json"
            )
            if inform in obj.keys():
                # lets make sure the object is a source string
                return obj[inform]
            else:
                return infodefault


async def one_action_for_one_component(oneresult, mysource_changes):

    logdate = oneresult['timestamp'].split('T')[0]
    action = oneresult['action_name']

    onechange = { 'action': action, 'date': logdate }

    if logdate not in mysource_changes.keys():
        mysource_changes[logdate] = {}

    if action not in mysource_changes[logdate].keys():
        mysource_changes[logdate][action] = []

    for newvalue in [
                "unit",
                "user",
                "author",
                "url",
                "id",
            ]:

        onechange[newvalue] = oneresult[newvalue]
    onechange['actionnr'] = oneresult['action']
    onechange['date'] = oneresult['timestamp'].split("T")[0]
    onechange["component"] = oneresult["component"].split("/")[6]
    if oneresult['unit'] != None:
        onechange['source_words'] = await get_source_info(oneresult['unit'], 'num_words', 0)
        onechange['source_url'] = await get_source_info(oneresult['unit'], 'web_url', None)

    else:
        onechange['source_words'] = 0

    if oneresult['translation'] != None:
        onechange['language'] = oneresult['translation'].split('/')[7]
        onechange['translation'] = oneresult['translation']
    else:
        onechange['language'] = '-'

    if oneresult['target'] not in ['-', None, '']:
        onechange['target'] = oneresult['target']
    else:
        onechange['target'] = '-'

    mysource_changes[logdate][action].append(onechange)
    if oneresult['id'] in component_changes:
        print(f'action was already in component_changes!')
        print( onechange['url'] )

    component_changes[oneresult['id']] = onechange


    return mysource_changes


def build_onecat_subcomponents_info(one_cat):
    # to add some more information to the header of a category
    cat_subcomponents = component_categories[one_cat]['subc']
    md_output = ''
    title = f'\n### Weblate components for { one_cat }\n'
    for cat_subc in cat_subcomponents:
        md_output += f' { comp_link(cat_subc) }'
    if md_output != '':
        write_comp_source_stats(one_cat, title)
        write_comp_source_stats(one_cat, md_output)


def add_changes_to_category(subcomponent, onechangedict, categ):
    global component_categories
    if categ not in component_categories.keys():
        component_categories[categ] = { 'subc': [] , 'changes': [] }
    if subcomponent not in component_categories[categ]['subc']:
        component_categories[categ]['subc'].append(subcomponent)
    if onechangedict['id'] not in component_categories[categ]['changes']:
        component_categories[categ]['changes'].append(onechangedict['id'])

async def gather_changes_for_category(onecategory):
    global component_changes
    global component_categories
    #comps_list = component_categories[onecategory]['subc']
    print(f'Gathering changes for { onecategory }')
    mycategory = component_categories[onecategory]
    for onechange in component_changes.values():
        if onechange['component'] in component_categories[onecategory]['subc']:
            add_changes_to_category(onechange['component'], onechange, onecategory)

async def added_strings_cat(specific_category, unfilteredchanges):
    # to filter the added strings for a category source files,
    # and add the stats to the category page
    headings = ['Date', 'Action', 'Component', 'Strings affected', 'Source wordcount']
    outtext = ''
    all_added_strings_for_cat = 0
    all_added_words_for_cat = 0
    for mydate, real_changes in groupby(unfilteredchanges, key=itemgetter('date')):
        # we separate first by date
        for actiontype, rchanges in groupby(real_changes, key=itemgetter('action')):
            # here we separate by action
            for action_component, per_comp_actions in groupby(rchanges, key=itemgetter('component')):
                # here we want the subcomponent name
                eventscount = 0
                source_w = 0
                for one_real_change in per_comp_actions:
                    if one_real_change['component'] in component_categories[specific_category]['subc']:
                    # just in case some other components sneaked on our list...
                    # we will have only the added strings to source
                        selected_actions = ['String added in the repository', 'String updated in the repository', 'Source string added']
                        if one_real_change['action'] in selected_actions:
                            if one_real_change['language'] == 'en':
                                eventscount += 1
                                all_added_strings_for_cat += 1
                                if 'source_words' in one_real_change:
                                    source_w += int(one_real_change['source_words'])
                                    all_added_words_for_cat += int(one_real_change['source_words'])
                            if 'source_words' in one_real_change and one_real_change['language'] != 'en':
                                print('source word changes in a translation? check plz:')
                                print(one_real_change['source_words'])

                if eventscount != 0:

                    datos = [ mydate , change_src_link(one_real_change), comp_link(one_real_change['component']), eventscount, source_w ]
                    outtext += table_line(datos)

    if outtext != '':

        headtext = f"\n### Added strings  { nice_changes_from } - { nice_date_built }\n"
        headtext += f"#### Total: { all_added_strings_for_cat } strings, { all_added_words_for_cat } words\n"
        headtext += table_headings(headings)
        write_comp_source_stats(specific_category, headtext)
        write_comp_source_stats(specific_category, outtext)

async def other_events_cat(onecat, unfilteredchanges):
    # to filter the added strings for a category source files,
    # and add the stats to the category page
    headtext = f"\n### Other interesting events since { nice_changes_from } until { nice_date_built }\n"
    headings = ['Date', 'Action', 'Component', 'Language', '#']
    headtext += table_headings(headings)
    outtext = ''
    for mydate, real_changes in groupby(unfilteredchanges, key=itemgetter('date')):

        for actiontype, rchanges in groupby(real_changes, key=itemgetter('action')):
            eventscount = 0
            source_w = 0
            lang_group = []
            for one_real_change in rchanges:
            # we select only the important actions
                selected_actions = ['Translation completed', 'Contributor joined', 'Translation uploaded', 'Alert triggered', 'Announcement posted']
                if one_real_change['action'] in selected_actions:
                    if 'restricted_to' in resources_config[onecat]:
                        if ( one_real_change['language'] in resources_config[onecat]['restricted_to'] ) or ( one_real_change['language'] in ['-', 'en']) :
                            eventscount +=1
                            lang_group.append(one_real_change['language'])
                    else:
                        eventscount +=1
                        lang_group.append(one_real_change['language'])
            if eventscount != 0:
                actionurl = f"[{ actiontype }](https://hosted.weblate.org/changes/browse/tor/?action={ one_real_change['actionnr'] }"
                actionurl += f"&start_date={ mydate }&end_date={ mydate })"
                languages_act = ", ".join(set(lang_group))
                datos = [ mydate , actionurl, comp_link(one_real_change['component']), languages_act, eventscount, source_w ]
                outtext += table_line(datos)
    if outtext != '':
        write_comp_source_stats(onecat, headtext)
        write_comp_source_stats(onecat, outtext)

async def sum_one_cat_changes(onecat):
    # lets run some calculations for categories, instead of components
    real_changes_list = []
    write_comp_source_stats(onecat, f"\n## Changes for all components\n")
    for onechange in component_categories[onecat]['changes']:
        real_changes_list.append(get_change_with_id(onechange))

    real_changes_list.sort(key=itemgetter('date','action', 'component'), reverse=True )

    await added_strings_cat(onecat, real_changes_list)
    await other_events_cat(onecat, real_changes_list)
    mdpath = f'stats/c-adm-{ onecat }.md'
    htmlpath = f'public/c-adm-{ onecat }.html'
    make_html(mdpath, htmlpath)



async def sum_category_changes():
    # calculates the changes per category, with all the subcomponents
    global component_categories
    global component_changes
    print('# check the changes first:')
    for onlyonecat in component_categories.keys():
        await gather_changes(onlyonecat)
        build_onecat_subcomponents_info(onlyonecat)
        await sum_one_cat_changes(onlyonecat)

def added_strings(list_of_changes, onecomponent):
    # search for new strings added to a component, and log if found
    global component_changes

    title1 = f"\n### Added strings"
    headings = ['Date', 'Action', 'Strings affected', 'Source wordcount']
    logtext = f"\n{ table_headings(headings) }"
    useful_data = ''
    for change_day in sorted(list_of_changes, reverse=True):
    # this are changes with wordcount (new strings added)
        for change_action in list_of_changes[change_day].keys():
            wordcount = 0
            stringscount = 0
            for onechangeaction in list_of_changes[change_day][change_action]:
                if onechangeaction['component'] == onecomponent:
                    stringscount += 1

                    if onechangeaction['source_words'] not in [0, '0', None]:
                        wordcount += int(onechangeaction['source_words'])

            if wordcount not in [0, '0']:
                action_link = change_src_link(onechangeaction)
                newline = [change_day, action_link, stringscount, wordcount]
                useful_data += table_line(newline)
    if useful_data != '':
        write_comp_source_stats(onecomponent, title1)
        write_comp_source_stats(onecomponent, logtext)
        write_comp_source_stats(onecomponent, useful_data)

def other_source_changes(list_of_changes, onecomponent):
    # this changes are about the source content, but have no strings
    global component_changes
    headings = ['Date', 'Action', 'Strings affected']
    logtext = f"\n### Other source changes\n{ table_headings(headings) }"
    useful_data = ''
    for change_day in sorted(list_of_changes, reverse=True):
    # this are changes with wordcount (new strings added)
        print(change_day)
        for change_action in list_of_changes[change_day].keys():
            stringscount = 0
            for onechangeaction in list_of_changes[change_day][change_action]:
                if onechangeaction['source_words'] in [0, '0', None]:
                    if onechangeaction['component'] == onecomponent:
                        stringscount += 1

            if stringscount not in [0, '0']:
                actionurl = f"[{ change_action }](https://hosted.weblate.org/changes/browse/"
                actionurl += f"tor/{ onechangeaction['component'] }/?action={ onechangeaction['actionnr'] }"
                actionurl += f"&start_date={ change_day }&end_date={ change_day })"
                newline = [change_day, actionurl, stringscount]
                useful_data += table_line(newline)
                component_changes[onechangeaction['id']] = onechangeaction
    if useful_data != '':
        write_comp_source_stats(onecomponent, logtext)
        write_comp_source_stats(onecomponent, useful_data)


async def get_new_strings_added_to_component(onecomponent):
    # we search for events about the source strings to add and build a timeline for the component
    # more from the point of view of the mantainers of the translation
    # new source strings added. ref: https://gitlab.torproject.org/tpo/community/l10n/-/issues/40135#note_3034043

    source_changes = {}
    action_list = '13 30 38 46 48 49 51 59 60 61 62 63 71 74'
    obj = await api(
            f"/api/translations/tor/{ onecomponent }/en/changes/?action={ '&action='.join(action_list.split(' ')) }&timestamp_after={ changes_from }&order_by=timestamp&format=json"
        )
    #gather some data

    if obj != None:
        if obj['count'] != '0':
            myheader  = '\n## Source string changes\n'
            write_comp_source_stats(onecomponent, myheader)
            for res in obj["results"]:
                await one_action_for_one_component(res, source_changes)

                while obj["next"] != None:
                    newpage = obj["next"].split("/")[-1]
                    obj = await api(
                        f"/api/translations/tor/{ onecomponent }/en/changes/{ newpage }"
                    )
                    for res in obj["results"]:
                        await one_action_for_one_component(res, source_changes)
            added_strings(source_changes, onecomponent)

            other_source_changes(source_changes, onecomponent)


async def interesting_component_events(onecomponent):
    # here we can also log other events from the component,
    # not related to the source changes
    # nor interesting for translators/reviewers
    global component_changes
    source_changes = {}
    action_list = '1 9 25 45 46 47 49 54 60 62 64 65 74'
    #ignored_actions in language for translators query= [17, 18, 21, 25, 26, 29, 40, 47, 53, 56, 59, 60, 61, 62, 64 ]
    obj = await api(
            f"/api/components/tor/{ onecomponent }/changes/?action={ '&action='.join(action_list.split(' ')) }&timestamp_after={ changes_from }&order_by=timestamp&format=json"
        )
    #gather some data

    if obj != None:
        if obj['count'] != '0':
            myheader  = '\n## Other interesting admin events\n'
            write_comp_source_stats(onecomponent, myheader)
            for res in obj["results"]:
                await one_action_for_one_component(res, source_changes)

                while obj["next"] != None:
                    newpage = obj["next"].split("/")[-1]
                    obj = await api(
                        f"/api/components/tor/{ onecomponent }/changes/{ newpage }"
                    )
                    for res in obj["results"]:
                        await one_action_for_one_component(res, source_changes)

            headings = ['Date', 'Action', 'lang/details', 'Strings affected']
            write_comp_source_stats(onecomponent, table_headings(headings))
            for change_day in sorted(source_changes, reverse=True):
            # this are events not related to the source content
            # but other admin stuff (add-ons, completed translations, new contributors
                for change_action in source_changes[change_day]:
                    list_actions = source_changes[change_day][change_action]
                    list_actions.sort(key=itemgetter('language'))
                    non_lang_actions = ['Add-on installed', 'Add-on uninstalled']
                    lang_group = []
                    for languag, actionlist in groupby(list_actions, key=itemgetter('language')):
                        stringscount = 0
                        for onechangeaction in actionlist:
                            onechangeaction['date'] = change_day
                            if change_action in non_lang_actions:
                                languag = onechangeaction['target']
                            if change_action == "Translation completed":
                                lang_group.append(languag)
                            stringscount += 1
                            actionurl = f"[{ change_action }](https://hosted.weblate.org/changes/browse/tor/"
                            actionurl += f"{ onechangeaction['component'].replace('%252F', '/') }/?action="
                            actionurl += f"{ onechangeaction['actionnr'] }&start_date={ change_day }&end_date={ change_day })"
                        if stringscount != 0:
                            newline = table_line(( change_day, actionurl, languag, stringscount ))
                            write_comp_source_stats(onecomponent, newline)
                        if onechangeaction['id'] not in component_changes.keys():
                            component_changes[onechangeaction['id']] = onechangeaction
                    if lang_group != []:
                        completed_line = table_line(( change_day, comp_link(onecomponent), ", ".join(set(lang_group)) ))
                        completed_path = f'stats/completed-translations.md'
                        write_logs(completed_path, completed_line)
            write_comp_source_stats(onecomponent, '\n')





async def get_one_component_changes(component_slug):
    # gets all changes for a component
    print(f'get component contributor stats of the last { contributors_data_range } days')
    #print(component)
    print('here we look for general activity for that component')
    for oneday in date_list:
        #action_list = '2 4 5 9 26 36'
        tomorrow = oneday + datetime.timedelta(days=1)

        action_list = '2 3 4 5 7 8 27 36 37 49 54'
    #ignored_actions = [17, 18, 21, 25, 26, 29, 40, 47, 53, 56, 59, 60, 61, 62, 64 ]
        obj = await api(
            f"/api/components/tor/{ component_slug }/changes/?action={ '&action='.join(action_list.split(' ')) }&order_by=timestamp&timestamp_after={ oneday.isoformat() }&timestamp_before={ tomorrow.isoformat() }&format=json"
        )
        if obj is not None:
            # for res in obj["results"][1:3]:
            for res in obj["results"]:
                await get_one_change_for_a_component(component_slug, res)
            while obj["next"] is not None:
                newpage = obj["next"].split("/")[-1]
                obj = await api(
                    f"/api/components/tor/{ component_slug }/changes/{ newpage }"
                )
                for res in obj["results"]:
                    await get_one_change_for_a_component(component_slug, res)

    print('now we should search for some actions for the timeline')

async def get_one_component_from_api( apiresult ):
    # lets create the admin stats page
    #lets get translations info from the API
    global component_changes
    thiscomponent = apiresult
    one_component_name = thiscomponent['url'].split('/')[6]
    #await get_translations(component_name)
    if one_component_name not in component_translations.keys():
        component_translations[one_component_name] = {"name": thiscomponent['name']}
    for dato in ['web_url', 'name'] :
        component_translations[one_component_name][dato] = thiscomponent[dato]
    #await get_one_component_changes(one_component_name)
    await get_new_strings_added_to_component(one_component_name)
    await interesting_component_events(one_component_name)
    mdpath = f'stats/c-adm-{ one_component_name }.md'
    htmlpath = f'public/c-adm-{ one_component_name }.html'
    make_html(mdpath, htmlpath)


async def get_all_components():
    # updates information for each component we have in weblate

    # ask the list of components to weblate
    obj = await api(f"/api/projects/tor/components/")

    for res in obj["results"]:
        await get_one_component_from_api( res)

        while obj["next"] != None:
            newpage = obj["next"].split("/")[-1]
            obj = await api(f"/api/projects/tor/components/{newpage}")
            for res in obj["results"]:
                await get_one_component_from_api( res)

def add_lang_release(component, lang_code):
    global component_translations
    if component not in component_translations.keys():
        component_translations[component] = {}
    if lang_code not in component_translations[component].keys():
        component_translations[component][lang_code] = {}
    component_translations[component][lang_code]["released"] = True

def get_change_with_id(change_id):
    # get a change with an id
    global component_changes

    try:
        change = component_changes[change_id]
        return change
    except:
        return None

def get_releases():
    print("\n Adding release info")
    for onlyonecomponent in resources_config.sections():
        if "components" not in resources_config[onlyonecomponent].keys():
            if "released" in resources_config[onlyonecomponent].keys():
                langs = resources_config[onlyonecomponent]["released"].split(", ")
                comp_header = "\n## Currently released Languages\n"
                write_comp_source_stats(onlyonecomponent, comp_header)
                write_comp_source_stats(onlyonecomponent, resources_config[onlyonecomponent]["released"])
                for lang_code in langs:
                    add_lang_release(onlyonecomponent, lang_code)

async def get_categories():
    print("\nCreating the categories list")
    # we populate a dict with the categories from the config file
    for supercomponent in resources_config.sections():
        if "components" in resources_config[supercomponent]:
            print(f'Found category: { supercomponent }.')
            write_comp_source_stats(supercomponent, page_header(f'Admin history for { supercomponent }\n'))
            if supercomponent not in component_categories:
                component_categories[supercomponent] = { 'subc': [], 'changes': [] }
            for subcomponent in resources_config[supercomponent]["components"].split(", "):
                if subcomponent not in component_categories[supercomponent]['subc']:
                    component_categories[supercomponent]['subc'].append(subcomponent)
            if "released" in resources_config[supercomponent]:
                comp_header = "\n## Translations\n"
                write_comp_source_stats(supercomponent, comp_header)
                write_comp_source_stats(supercomponent, resources_config[supercomponent]['released'])
                for subcomponent in resources_config[supercomponent]["components"].split(", "):
                    write_comp_source_stats(subcomponent, comp_header)
                    write_comp_source_stats(subcomponent, resources_config[supercomponent]['released'])
                    langs = resources_config[supercomponent]['released'].split(", ")
                    for lang_code in langs:
                        add_lang_release(subcomponent, lang_code)



def create_common_md_pages():
    # to add a title and TOCs to the common pages
    write_logs('stats/admin-stats.md', page_header("Mantainer stats"))
    for ptitle, ppath in [
        ( "Finished translations", 'stats/completed-translations.md'),
        ( "All new strings added to translation", 'stats/added_strings.md'),
        ]:
        write_logs(ppath, f'\n## { ptitle }\n')

    theadings = ['Date', 'Component', 'Lang']
    for ppath in [ 'stats/completed-translations.md']:
        write_logs(ppath, table_headings(theadings))
    theadings = ['Date', 'Component', 'strings', 'words']
    write_logs('stats/added_strings.md', table_headings(theadings))


async def main():
    global api_token, languages

    if len(sys.argv) != 2:
        print(f"Usage: {sys.argv[0]} API_KEY")
        print(
            "You can find your personal API key at: https://hosted.weblate.org/accounts/profile/#api"
        )
        return

    api_token = sys.argv[1]
    create_common_md_pages()
    # Get the list of languages in the project
    res = await api("/api/projects/tor/languages/")
    for obj in res:
        if obj["code"] != "en":
            languages[obj["code"]] = obj
            try:
                languages[obj["code"]]["last_change"] = ":".join(
                    obj["last_change"].split(":")[:2]
                ).replace("T", " ")
            except AttributeError:
                languages[obj["code"]]["last_change"] = None

    # start building the pages for each component and category
    await get_all_components()
    await get_categories()
    get_releases()
    for onlyonecat in component_categories.keys():
        print(onlyonecat)
        # to add some more information to the header of a category
        cat_subcomponents = component_categories[onlyonecat]['subc']
        md_output = ''
        title = f'\n### Weblate components for { onlyonecat }\n'
        for cat_subc in cat_subcomponents:
            md_output += f' { comp_link(cat_subc) }'
        if md_output != '':
            write_comp_source_stats(onlyonecat, title)
            write_comp_source_stats(onlyonecat, md_output)

        await gather_changes_for_category(onlyonecat)
        await sum_one_cat_changes(onlyonecat)


    # convert markdown to html
    # first gather some markdown files in bigger md files
    # for the TOC to work
    filenames = [
        "stats/admin-stats.md",
        "stats/completed-translations.md",
        "stats/added_strings.md",
    ]
    with open("stats/all-stats.md", "w") as outfile:
        for fname in filenames:
            with open(fname) as infile:
                for line in infile:
                    outfile.write(line)

    make_html("stats/all-stats.md", "public/admin-stats.html")

    #print(component_changes['Translation added'])
    #print(component_categories.items())
    #await get_general_week_activity()
    #print(component_translations['glossary'])

if __name__ == "__main__":
    asyncio.run(main())
