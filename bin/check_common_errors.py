#!/usr/bin/env python3

# this script checks for the most common error in markdown translations:
# Links should be [word](link).
# written by emmapeel, sponsored by the Tor Project.
# most is copy-paste from intrigeri's tails gitlab
# scripts: https://gitlab.tails.boum.org/tails/jenkins-tools/-/blob/master/slaves/lint_po
# this script is published under the GPL v3 license.
# requires debian packages: python3-polib, gettext
'''
This script checks a directory with .po files for errors with links.
Translation errors with links are the most common errors on the
Tor Project websites.

Usage:

./check_common_errors.py ../translation/

'''
import sys
import os

try:
    import polib
except ImportError:
    sys.exit("You need to install python3-polib to use this program.")

try:
    FOLDER = sys.argv[1]
except IndexError:
    FOLDER = os.getcwd()

RTL_LANGUAGE_TEAMS = ['Hebrew', 'Arabic', 'Persian', 'Urdu', 'Pashto']

def make_string_link(source_string,pofile):
    'URL generator to search for a string in transifex'
    lang_code = pofile.fpath.split('+')[1].split('.')[0].replace("-", "_")
    if lang_code == 'nb_NO':
        lang_code = 'nb'
    if lang_code == 'ms':
        lang_code = 'ms_MY'
    if lang_code == 'sv':
        lang_code = 'sv_SE'
    source_string_modified = source_string
    #we need to replace some chars that are not allowed on the url
    source_string_modified = source_string_modified.replace('%', "%25")
    source_string_modified = source_string_modified.replace('"', '%5C%22')
    source_string_modified = source_string_modified.replace('#', "%23")
    source_string_modified = source_string_modified.replace('/', "%2F")
    source_string_modified = source_string_modified.replace('=', "%3D")
    source_string_modified = source_string_modified.replace('&', "%26")
    source_string_modified = source_string_modified.replace(':', '%3A')
    string_link =  f"<a href=\"https://hosted.weblate.org/languages/{lang_code}/tor/search/?offset=1&q=source%3A%22{source_string_modified}%22+&sort_by=-priority%2Cposition&checksum=\">fix</a>"

    return string_link


def get_alt(po_string):
    'we parse the contents of an alt attribute'
    try:
        return po_string.split('alt=')[1].split('"')[1]
    except IndexError():
        print(f'please check {po_string}')


def check_strings(pofile):
    'performs several checks in one particular po file'
    language_team = ' '.join(pofile.metadata['Language-Team'].split(' ')[0:-1])
    def add_to_list(entry, error_list, pofile):
        'generates a line to paste on markdown'
        'with an html link to search for the string'
        message = f"\n{entry.msgstr} - {make_string_link(entry.msgid,pofile)}"
        if message not in error_list:
            error_list.append(message)

    def add_img_to_list(entry, error_list, pofile):
        'generates a line to paste on markdown'
        'with an html link to search for the string'
        message = f"\n&#x202a;{entry.msgstr.replace('<','&lt;')[-90:]} - {make_string_link(entry.msgid,pofile)}"
        if message not in error_list:
            error_list.append(message)

    def report_one_check(errors_links, title, help_link, language_team):
        if language_team in RTL_LANGUAGE_TEAMS:
            report_string = f'\n<h3 style="text-align: right">{ title } <a href="{help_link}">(?)</a></h3>'
            report_string += '\n<ul dir="rtl" style="direction: rtl; text-align: right">'
            report_string += '\n<li>'
            report_string += '</li>\n<li dir="rtl">'.join(errors_links)
            report_string += '\n</ul>'

        if language_team not in RTL_LANGUAGE_TEAMS:
            report_string = f'\n<h3>{ title } <a href="{help_link}">(?)</a></h3>'
            report_string += '\n<ul>'
            report_string += '\n<li>'
            report_string += '</li>\n<li>'.join(errors_links)
            report_string += '\n</ul>'

        report_string += '\n'
        return report_string


    results_in_markdown = '\n'

    # this are the lists where we are going to gather the errors
    broken_links = []
    missing_quotes = []
    different_pillows = []
    untranslated_alts = []
    missing_access_keys = []

    # now we go through the strings and see if there are issues
    # see better explanations of errors clicking the (?) signs at
    # https://tpo.pages.torproject.net/community/l10n/
    for entry in pofile.translated_entries():
        # we only want the strings marked as translated, not
        # the fuzzy or graveyard ones
        if not entry.obsolete or entry.fuzzy:
            if entry.msgid.count("](") != entry.msgstr.count("]("):
                add_to_list(entry, broken_links, pofile)
            if entry.msgid.count("(../") != entry.msgstr.count("(../"):
                add_to_list(entry, broken_links, pofile)
            if entry.msgid.count("`") != entry.msgstr.count("`"):
                add_to_list(entry, missing_quotes, pofile)
            if entry.msgid.count("#") != entry.msgstr.count("#"):
                if entry.msgid[1] == '#':
                    add_to_list(entry, different_pillows, pofile)
            if entry.msgid.count("_") != entry.msgstr.count("_"):
                if entry.msgid.count("_") == 1:
                    add_to_list(entry, missing_access_keys, pofile)
            if entry.msgstr.count("alt=\"") != 0:
                if get_alt(entry.msgstr) == get_alt(entry.msgid):
                    add_img_to_list(entry, untranslated_alts, pofile)

    # now lets analize the results

    if broken_links != [] or missing_quotes != [] or different_pillows != [] or untranslated_alts != [] or missing_access_keys != []:
        # if we see problems with a language, we start a section on the report
        print(language_team)
        if language_team in RTL_LANGUAGE_TEAMS:
            results_in_markdown += f'\n<h3 style="text-align: right">{ language_team }</h3>'
        if language_team not in RTL_LANGUAGE_TEAMS:
            results_in_markdown += f'\n<h3>{ language_team }</h3>'


    # here we analize each test and write on the report
    if broken_links != []:
        help_link = "https://gitlab.torproject.org/tpo/community/l10n/-/wikis/Localization-for-translators#links"
        title = f'Broken links: { len(broken_links) }'
        print(title)
        results_in_markdown += report_one_check(broken_links, title, help_link, language_team)

    if missing_quotes != []:
        help_link = "https://gitlab.torproject.org/tpo/community/l10n/-/wikis/Localization-for-translators#the-quotes-with-"
        title = f'Missing quotes: { len(missing_quotes) }'
        print(title)
        results_in_markdown += report_one_check(missing_quotes, title, help_link, language_team)

    if different_pillows != []:
        help_link = "https://gitlab.torproject.org/tpo/community/l10n/-/wikis/Localization-for-translators#the-pillows-"
        title = f'Problems with pillows: { len(different_pillows) }'
        print(title)
        results_in_markdown += report_one_check(different_pillows, title, help_link, language_team)

    if missing_access_keys != []:
        help_link = "https://gitlab.torproject.org/tpo/community/l10n/-/wikis/Localization-for-translators#access-keys"
        title = f'Problems with Access Keys: { len(missing_access_keys) }'
        print(title)
        results_in_markdown += report_one_check(missing_access_keys, title, help_link, language_team)

    if untranslated_alts != []:
        help_link = "https://gitlab.torproject.org/tpo/community/l10n/-/wikis/Localization-for-translators#the-alt-attribute"
        title = f'Images "alt" attributes not translated: { len(untranslated_alts) }'
        print(title)
        results_in_markdown += report_one_check(untranslated_alts, title, help_link, language_team)

    return results_in_markdown


def go_trough_files():
    'open each .po file and check for broken links'
    for f in os.listdir(FOLDER):
        if f.endswith('.po') and not f.endswith('+en.po'):
            'we only want to check the .po files on the folder'
            po = polib.pofile(FOLDER+'/'+f, encoding='UTF-8')
            file_results = check_strings(po)
            if file_results != '\n':
                with open("../problems.md", "a") as output_file:
                    output_file.write(file_results)
        else:
            'probably some other file on the repo'
            pass

def main():
    'we walk the files checking for broken links and we write the results to file'
    go_trough_files()

main()
