#!/bin/bash
# this script takes the url of a gitlabCI build job
# from lektor, and replaces the current translation template.
# and updates the current contents.pot file we have
# keeping the same order so we dont spam git and
# can see the changes more clearly

# it needs to be run on the translation branch of the project,
# inside the src/ directory
rm contents.pot.*
wget $1/artifacts/raw/i18n/contents.pot
#mv contents.pot.1 contents.pot
msgmerge -UF contents.pot contents.pot.1
# here we remove the strings present on the 'ignore.pot' file
# (mainly commands)
msgattrib --set-obsolete --only-file=ignore.pot contents.pot -o contents.pot
rm contents.pot.1
# here we remove some of the boilerplate, not sure how to do it directly in lektor-i18n plugin
sed -i 's/Report-Msgid-Bugs-To: \\n/Report-Msgid-Bugs-To: tor-l10n@lists.torproject.org\\n/g' contents.pot
git diff contents.pot
