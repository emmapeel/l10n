#!/usr/bin/env python3

'''
This script looks for translation templates that we update from a URL.
For example, we update the file src/tails.pot on the 'tails-misc'
branch from https://gitlab.tails.boum.org/tails/tails/-/raw/devel/po/tails.pot

written by emmapeel, sponsored by the Tor Project.
this script is published under the GPL v3 license.
The part about gitlab alerts was stolen from lavamind

Usage:

./bin/check-remote-updates.py

'''
import sys
import os
import logging
import filecmp
import configparser
import requests


try:
    resources_config = configparser.ConfigParser()
    resources_config.read("./configs/remote-sources.ini")
except IndexError:
    print('Error: did not find config file!')
    logging.warning("Please run me on the root directory of the community/l1n repository")
    sys.exit()


def get_upstream_file(component):
    '''
    we download a translation source file from our translation repo so we can
    compare it to the file we have as source in our repository
    '''
    upstream_file = resources_config[component]['upstream_source']

    r = requests.get(upstream_file, allow_redirects=True, timeout=30)
    try:
        open('upstream_file', 'wb').write(r.content)
    except:
        print(f"could not download the upstream source file. are you sure it is located at { upstream_file }?")
        sys.exit()

def get_current_file(onecomponent):
    ''' this is the file we currently have for translation'''
    filename = resources_config[onecomponent]['current_template']
    repo_branch = resources_config[onecomponent]['repo_branch']
    #https://gitlab.torproject.org/tpo/translation/-/raw/onionsproutsbot/src/onionsproutsbot.pot?ref_type=heads

    current_file = f'https://gitlab.torproject.org/tpo/translation/-/raw/{ repo_branch }/{ filename }?ref_type=heads'

    r = requests.get(current_file, allow_redirects=True, timeout=30)
    try:
        open('current_file', 'wb').write(r.content)
    except:
        print(f"could not download the upstream source file. are you sure it is located at { current_file }?")


def compare_files():
    # compare the two files
    compare = filecmp.cmp('upstream_file', 'current_file')
    return compare

print('CHECK REMOTE UPDATES')
print('--------------------')

for component in resources_config.sections():
    # we go through the components listed in the config file

    print(f"Checking { component }...")
    # we download the files from upstream and weblate
    get_upstream_file(component)
    get_current_file(component)
    # compare the files
    compare = compare_files()
    os.remove('upstream_file')
    os.remove('current_file')
    repobranch = resources_config[component]['repo_branch']
    upstream = resources_config[component]['upstream_source']
    current = resources_config[component]['current_template']
    
    # this is to generate an alert in https://gitlab.torproject.org/tpo/community/l10n/-/alert_management
    # copied from the one lavamind did in the check_po_status script on this folder.
    alert_endpoint = os.environ.get('L10N_ALERT_ENDPOINT')
    alert_token = os.environ.get('L10N_ALERT_TOKEN')

    if compare is False:
        # there are changes, we create an alert
        alert_description = f'--- { component } --- is outdated! On the translation repository- git checkout {repobranch} && git pull && wget { upstream } -O { current } && git diff  ---then, review and:   git add { current } && git commit -m "translation template updated" && git push'
        print(alert_description)
        if alert_endpoint:
            alert_payload = { 'title': f'{component} translation template needs update',
                              'description': alert_description,
                              'severity': 'info',
                              'fingerprint': component }
            alert_auth = {'Authorization': f'Bearer {alert_token}', 'Content-Type': 'application/json'}
            requests.post(alert_endpoint, json=alert_payload, headers=alert_auth, timeout=30)

    if compare is True:
        # we remove the alert
        if alert_endpoint:
            from datetime import datetime
            alert_payload = { 'title': f'{component} translation template needs update',
                              'severity': 'info',
                              'end_time': datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f'),
                              'fingerprint': component }
            alert_auth = {'Authorization': f'Bearer {alert_token}', 'Content-Type': 'application/json'}
            requests.post(alert_endpoint, json=alert_payload, headers=alert_auth, timeout=30)
