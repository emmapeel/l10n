#!/usr/bin/env python3
# this script is based/copied from
# Micah Lee's script for OnionShare
# translations at
# https://github.com/onionshare/onionshare/blob/main/docs/check-weblate.py
# modified by emmapeel for the Tor Project - 2023
import sys
import httpx
import asyncio
import markdown
import datetime
import configparser
import os.path
import json

resources_config = configparser.ConfigParser()

resources_config.read("./ci-templates/stats/resources-config.ini")


api_token = None
languages = {}
ignored_languages = "pbb"
component_translations = {}
component_changes = {}

dt = datetime.datetime.now()
date_built = dt.strftime("%A, %d. %B %Y %I:%M%p")
missing_translations = {}
help_needed = {}


# def get_published_langs(lektorconfig):
# gets a list of all locales configured on a particular Lektor website
# needs a lektor i18n config file, such as the one available at
# https://gitlab.torproject.org/tpo/web/l10n/community/-/blob/main/configs/i18n.ini
#    mylangs = []
#    with open(lektorconfig, "rt") as config_file:
#        for config_option in config_file.readlines():
#            if config_option.startswith("translations"):
#                mylangs = [
#                    name.strip()
#                    for name in config_option.split("=")[1].split(",")
#                ]
#    return mylangs


async def api(path):
    global languages
    url = f"https://hosted.weblate.org{path}"

    # lets cache the json files to reduce requests to the server (green computing!)
    cached_file = f'./stats/{"-".join( path.split("/")[2:]).replace("api/","").replace("-tor-","-").replace("&action=","=").replace("components-","comp-") }.json'
    if os.path.exists(cached_file):
        with open(cached_file) as f:
            data = json.load(f)
            return data
    else:

    # Wait a bit before each API call, to avoid hammering the server and
    # getting temporarily blocked
        await asyncio.sleep(1)
        async with httpx.AsyncClient() as client:
            r = await client.get(
                url,
                headers={"Authorization": f"Token {api_token}"},
                timeout=60,
            )

        if r.status_code == 200:
            print(f"GET {url}")
            with open(cached_file, "w") as output_file:
                json.dump(
                    r.json(),
                    output_file,
                    sort_keys=True,
                    ensure_ascii=False,
                    indent=4,
                )

            return r.json()
        else:
            print(f"GET {url} | error {r.status_code}")
            return None


async def add_stats_to_component(obj, component):
    # process a json result object line and
    # adds the translation percentage to the results
    global component_translations
    for res in obj:
        if res["code"] != "en":

            if not res["code"] in component_translations[component]:
                component_translations[component][res["code"]] = {}
            component_translations[component][res["code"]]["released"] = False
            try:
                component_translations[component][res["code"]][
                    "last_change"
                ] = datetime.strptime(
                    res["last_change"], "%Y-%m-%dT%H:%M:%S.%f%z"
                )
            except AttributeError:
                component_translations[component][res["code"]][
                    "last_change"
                ] = None
            for newvalue in [
                "total",
                "total_words",
                "translated",
                "translated_percent",
                "fuzzy",
                "fuzzy_percent",
                "approved",
                "recent_changes",
                "failing",
                "suggestions",
                "comments",
                "translate_url",
            ]:
                component_translations[component][res["code"]][newvalue] = res[
                    newvalue
                ]


async def get_translations(component):
    global component_translations

    obj = await api(f"/api/components/tor/{component}/statistics/")
    await add_stats_to_component(obj["results"], component)

    while obj["next"] != None:
        newpage = obj["next"].split("/")[-1]
        obj = await api(
            f"/api/components/tor/{component}/statistics/{newpage}"
        )
        await add_stats_to_component(obj["results"], component)


async def get_all_components():
    # updates information for all components for tor project
    global component_translations
    obj = await api(f"/api/projects/tor/components/")
    # for res in obj["results"][1:3]:
    for res in obj["results"]:
        component_name = res["url"].split('/')[-2]
        print('component_name')
        print(component_name)

        if component_name not in component_translations:
            component_translations[component_name] = {"name": res["name"]}
        await get_translations(component_name)
        #await get_one_component_changes(component_name)
        if component_name not in resources_config.keys():
            print(
                f'please add { component_name } to the config file at "./ci-templates/stats/resources-config.ini'
            )

    while obj["next"] != None:
        newpage = obj["next"].split("/")[-1]
        obj = await api(f"/api/projects/tor/components/{newpage}")
        for res in obj["results"]:
            component_name = res["url"].split('/')[-2]
            await get_translations(component_name)
            await get_one_component_changes(component_name)

    

def add_lang_release(component, lang_code):
    global component_translations
    if lang_code not in component_translations[component]:
        component_translations[component][lang_code] = {}
    component_translations[component][lang_code]["released"] = True


def get_releases():
    global component_translations
    print("\n Adding release info")
    for component in resources_config.sections():
        if "released" in resources_config[component]:
            langs = resources_config[component]["released"].split(", ")
            for lang_code in langs:
                if "components" in resources_config[component]:
                    for subcomponent in resources_config[component][
                        "components"
                    ].split(", "):
                        add_lang_release(subcomponent, lang_code)
                else:
                    add_lang_release(component, lang_code)


async def one_lang_page(lang_code):
    # generates the page at stats/$lang.html
    out = []
    for component in component_translations:
        if lang_code in component_translations[component].keys() and "suggestions" in component_translations[component][lang_code].keys():
            langstats = component_translations[component][lang_code]
            released = "|  ⋆" if langstats["released"] == True else "| "
            # we make a markdown table line for each component, and then we sort them
            suggestions = (
                "| 0 "
                if langstats["suggestions"] == 0
                else f" | [{ langstats['suggestions'] }]"
                + f"({ langstats['translate_url'] }"
                + "?q=has%3Asuggestion&sort_by=-priority%2Cposition&checksum=#suggestions) "
            )

            comments = (
                "| 0 "
                if langstats["comments"] == 0
                else f" | [{ langstats['comments'] }]({ langstats['translate_url'] }"
                + "?q=has%3Acomment&sort_by=-priority%2Cposition&checksum=#comments) "
            )

            failing = (
                "| 0 "
                if langstats["failing"] == 0
                else f" | [{ langstats['failing'] }]({ langstats['translate_url'] }"
                + "?q=has%3Acheck&sort_by=-priority%2Cposition&checksum=#translations) "
            )

            translated_percent = (
                f'| { langstats["translated_percent"] }% '
            )

            lastchanges = (
                "| 0 "
                if langstats["last_change"] == None
                else f"| { langstats['last_change'] }"
            )

            approvedstr = (
                "| 0 "
                if langstats["approved"] == 0
                else f"| [{ langstats['approved'] }]({ langstats['translate_url'] }"
                + "?q=state%3Aapproved&sort_by=-priority%2Cposition&checksum=#history) "
            )
            needsreview = (
                "| 0 "
                if (langstats["translated"] - langstats['approved']) == 0
                else f"| [{ langstats['translated'] - langstats['approved'] }]({ langstats['translate_url'] }"
                + "?q=state%3Atranslated&sort_by=-priority%2Cposition&checksum=#history) "
            )


            tr_line = (
                released
                + f"| { int(langstats['translated_percent']) }%"
                + f"| [{ component_translations[component]['name'] } ]({ langstats['translate_url'] })"
                + f"| { langstats['translated'] }/{ langstats['total'] }"
                + approvedstr
                + needsreview
                + str(failing)
                + str(suggestions)
                + str(comments)
                + translated_percent
                + str(lastchanges)
                + f"| { langstats['recent_changes'] }"
            )
            out.append(tr_line)
            if (
                langstats["released"] == True
                and langstats["translated_percent"] != 100.0
            ):
                if lang_code not in help_needed.keys():
                    help_needed[lang_code] = []
                help_needed[lang_code].append(component)
        else:
            if lang_code not in missing_translations and lang_code != "en":
                missing_translations[lang_code] = []
            missing_translations[lang_code].append(component)

    out.sort()

    with open(f"stats/lang-stats-{ lang_code }.md", "w") as output_file:
        output_file.write(
            f"\n# Translation status for { languages[lang_code]['name'] }"
        )
        output_file.write(f"\nbuilt on {date_built} (updates usually at 4amUTC every day)\n")
        output_file.write("\n[TOC]")
        if lang_code in help_needed.keys():
            output_file.write("\n### High Priority translations")
            output_file.write(
                "\nThis translations are released but the translation is not complete.\n\n Please update this translations first:\n"
            )
            for help_comp in help_needed[lang_code]:
                untranslated = str(int(component_translations[help_comp][lang_code]["total"])-int(component_translations[help_comp][lang_code]["translated"]))
                output_file.write(
                    f'\n- <a href="https://hosted.weblate.org/translate/tor/{ help_comp.replace("%252F","/") }/{ lang_code }/'
                    + f'?q=state%3A%3Ctranslated&sort_by=-priority%2Cposition&checksum=#machinery">{ component_translations[help_comp]["name"] }</a> '
                    + f'Unfinished strings: <a href="https://hosted.weblate.org/translate/tor/{ help_comp.replace("%252F","/") }/{ lang_code }/'
                    + f'?q=state%3A%3Ctranslated&sort_by=-priority%2Cposition&checksum=#machinery">{ untranslated } '
                    + f'<img src="https://hosted.weblate.org/widget/tor/{ help_comp.replace("%252F","/") }/{ lang_code }/svg-badge.svg" alt="{ help_comp.split("%252F")[-1] }" /></a> '
                )
            output_file.write("\n")

        output_file.write("\n### All Translations")
        output_file.write(
            "\n|Released?| %|Component | Translated| Approved | Needs Review | Failing | Suggestions | Comments |%| Last change | Recent changes|"
        )
        output_file.write(
                "\n|---:|---:|---------|---:|---:|---:|---:|---:|---|---|---|---|\n"
        )
        output_file.write("\n".join(out))
        output_file.write("\n")

        if lang_code in missing_translations:
            output_file.write("\n### Missing translations\n")
            for missing_component in missing_translations[lang_code]:
                output_file.write(
                    f'\n - <a href="https://hosted.weblate.org/projects/tor/{ missing_component }/">'
                    + f'{ component_translations[missing_component]["name"] }</a>'
                )
        # write an html page for the language
    from markdown.extensions.toc import TocExtension

    markdown.markdownFromFile(
        input=f"stats/lang-stats-{ lang_code }.md",
        extensions=[
            "markdown.extensions.tables:TableExtension",
            "md_in_html",
            TocExtension(toc_depth="2-3"),
        ],
        output=f"stats/{ lang_code }.html",
    )
    filenames = [
        "./ci-templates/stats-header.html",
        f"./stats/{ lang_code }.html",
        "./ci-templates/stats-footer.html",
    ]
    with open(f"./public/{ lang_code }.html", "w") as outfile:
        for fname in filenames:
            with open(fname) as infile:
                for line in infile:
                    outfile.write(line)


async def main():
    global api_token, languages

    if len(sys.argv) != 2:
        print(f"Usage: {sys.argv[0]} API_KEY")
        print(
            "You can find your personal API key at: https://hosted.weblate.org/accounts/profile/#api"
        )
        return

    api_token = sys.argv[1]

    # Get the list of languages in the project
    res = await api("/api/projects/tor/languages/")
    for obj in res:
        if obj["code"] != "en":
            languages[obj["code"]] = obj
            try:
                languages[obj["code"]]["last_change"] = ":".join(
                    obj["last_change"].split(":")[:2]
                ).replace("T", " ")
            except AttributeError:
                languages[obj["code"]]["last_change"] = None

    # start building the page
    print('Get information for all components in weblate')
    await get_all_components()
    get_releases()
    with open("stats/lang-stats.md", "w") as output_file:
        output_file.write("\n# Translations for the Tor Project\n")
        output_file.write("\n[TOC]\n")

        output_file.write("\n## Translation status per language\n")
        output_file.write(f"\nbuilt on {date_built} (updates usually at 4amUTC every day)\n")
        output_file.write(
            '<img src="https://hosted.weblate.org/widgets/tor/-/horizontal-auto.svg" height="200px" width="100%"/>\n'
        )
        output_file.write(
            "\n| Locale | Translated | % | Suggestions | Reviewed | Failing |Fuzzy|%|Recent changes|Last change|\n"
        )
        output_file.write(
            "| ------ | ----------:|---:|-----------:| --------:| -------:|---:|---|---:|---|\n"
        )

        # Get the Tor Project translations for each language and component
        for locale in languages:
            # we add a link and quick info
            langstats = languages[locale]
            suggestions = (
                "| 0 "
                if langstats["suggestions"] == 0
                else f" | [{ langstats['suggestions'] }]"
                + f"({ langstats['translate_url'] }"
                + "?q=has%3Asuggestion&sort_by=-priority%2Cposition&checksum=#suggestions) "
            )
            failing = (
                "| 0 "
                if langstats["failing"] == 0
                else f" | [{ langstats['failing'] }]({ langstats['translate_url'] }"
                + "?q=has%3Acheck&sort_by=-priority%2Cposition&checksum=#translations) "
            )

            fuzzy = (
                "| 0 "
                if langstats["fuzzy"] == 0
                else f" | [{ langstats['fuzzy'] }]({ langstats['translate_url'] }"
                + "?q=state%3Aneeds-editing&sort_by=-priority%2Cposition&checksum=#machinery) "
            )
            output_file.write(
                f"| [{ langstats['name'] }]({ locale }.html) "
                + f"| { langstats['translated'] }/{ langstats['total'] } "
                + f"| { langstats['translated_percent'] }% "
                + suggestions
                + f"| { langstats['approved'] } "
                + failing
                + fuzzy
                + f'| { langstats["translated_percent"] }% '
                + f"| { langstats['recent_changes'] } "
                + f"| { langstats['last_change'] } "
                + "|\n"
            )
            await one_lang_page(locale)
        # add released but outdated translations
        if len(help_needed) != 0:
            output_file.write("\n### High Priority translations")
            output_file.write(
                "\nThis translations are released but the translation is not complete.\n\n Please update this translations first\n"
            )
            for lang_code in languages:
                if lang_code in help_needed.keys():
                    output_file.write(
                        f"\n#### <a href='{ lang_code }.html' title='{ lang_code } language page'>{ languages[lang_code]['name'] }</a>"
                    )
                    for help_comp in help_needed[lang_code]:
                        output_file.write(
                            f' - <a href="https://hosted.weblate.org/translate/tor/{ help_comp.replace("%252F","/") }/{ lang_code }'
                            + f'/?q=state%3A%3Ctranslated&sort_by=-priority%2Cposition&checksum=#machinery" title="{ help_comp.split("%252F")[-1] }">'
                            + f'{ help_comp.split("%252F")[-1] }: { component_translations[help_comp][lang_code]["translated_percent"] }% </a> '
                        )
            output_file.write("\n")

    filenames = [
        "stats/lang-stats.md",
        "stats/tb-stats.md",
    ]
    with open(f"stats/all-stats.md", "w") as outfile:
        for fname in filenames:
            with open(fname) as infile:
                for line in infile:
                    outfile.write(line)

    # convert markdown to html
    from markdown.extensions.toc import TocExtension

    markdown.markdownFromFile(
        input="stats/all-stats.md",
        extensions=[
            "tables",
            "md_in_html",
            TocExtension(toc_depth="2-3"),
        ],
        output="stats/lang-stats.html",
    )


if __name__ == "__main__":
    asyncio.run(main())
