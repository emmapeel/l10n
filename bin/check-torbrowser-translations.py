#!/usr/bin/env python3
# this script is based/copied from
# Micah Lee's script for OnionShare
# translations at
# https://github.com/onionshare/onionshare/blob/main/docs/check-weblate.py
# modified by emmapeel for the Tor Project - 2023
import sys
import asyncio
import datetime
import configparser
import os.path
import json
import httpx

resources_config = configparser.ConfigParser()

resources_config.read("./ci-templates/stats/resources-config.ini")

api_token = None
languages = {}
tor_browser_translations = {}


async def api(path):
    global languages
    url = f"https://hosted.weblate.org{path}"
    cached_file = f'./stats/{"-".join( path.split("/")[2:]).replace("api/","").replace("-tor-","-").replace("&action=","=").replace("components-","comp-") }.json'

    # lets save the file in disk, to prevent unneeded requests
    if os.path.exists(cached_file):
        with open(cached_file) as f:
            data = json.load(f)
            return data
    else:

        # Wait a bit before each API call, to avoid hammering the server and
        # getting temporarily blocked
        await asyncio.sleep(1)
        async with httpx.AsyncClient() as client:
            r = await client.get(
                url,
                headers={"Authorization": f"Token {api_token}"},
                timeout=60,
            )

        if r.status_code == 200:
            print(f"GET {url}")
            with open(cached_file, "w") as output_file:
                json.dump(
                    r.json(),
                    output_file,
                    sort_keys=True,
                    ensure_ascii=False,
                    indent=4,
                )

            return r.json()
        else:
            print(f"GET {url} | error {r.status_code}")


async def add_stats_to_component(obj, component):
    # process a json result object line and
    # adds the translation percentage to the results
    global tor_browser_translations
    for res in obj:
        tor_browser_translations[component][res["code"]] = res[
            "translated_percent"
        ]


async def get_tb_translation(component):
    global tor_browser_translations

    if component not in tor_browser_translations:
        tor_browser_translations[component] = {}
    obj = await api(f"/api/components/tor/{component}/statistics/")

    if obj is None:

        print(f'WARNING: component { component } does not exist anymore! Please update "ci-templates/stats/resources-config.ini"')
    else:

        await add_stats_to_component(obj["results"], component)

        while obj["next"] is not None:
            newpage = obj["next"].split("/")[-1]
            obj = await api(
                f"/api/components/tor/{component}/statistics/{newpage}"
            )
            await add_stats_to_component(obj["results"], component)


async def docs_percent_output(percent_min, percent_max=101):
    out = []
    released_langs = [
        name.strip()
        for name in resources_config["tor-browser"]["released"].split(",")
    ]
    for lang_code in languages:
        percentages = []
        for component in tor_browser_translations:
            if lang_code in tor_browser_translations[component]:
                percentages.append(
                    tor_browser_translations[component][lang_code]
                )
            else:
                percentages.append(0)

        average_percentage = int(sum(percentages) / len(percentages))

        if (
            average_percentage != 0
            and average_percentage >= percent_min
            and average_percentage < percent_max
        ):
            if lang_code in released_langs:
                out.append(
                    f"|*| [{languages[lang_code][0]}](https://hosted.weblate.org/projects/tor/tor-browser/-/{lang_code}/) "\
                    + f"| {lang_code} | {languages[lang_code][1]}| {average_percentage}% |"
                )
            else:
                out.append(
                    f"| | [{languages[lang_code][0]}](https://hosted.weblate.org/projects/tor/tor-browser/-/{lang_code}/) "\
                    + f"| {lang_code} | {languages[lang_code][1]} | {average_percentage}% |"
                )

    out.sort()
    with open("stats/tb-stats.md", "a") as output_file:
        output_file.write(f"\n### {percent_min}% / {percent_max}%\n\n")
        output_file.write("| Release |Language | code | tbcode | Perc|")
        output_file.write("\n|---|----------|------|---|---|\n")
        output_file.write("\n".join(out))
        output_file.write("\n")

    print(f"Tor Browser translations >= {percent_min}%")
    print("==============================")
    print("\n".join(out))

    print("")


async def get_component_stats(obj):
    global languages
    # we don't need the English translations
    if obj["language_code"] != "en":
        # Lets just check translations that are more than 20%
        if obj["translated_percent"] >= float(10):
            languages[obj["language"]["code"]] = [
                obj["language"]["name"],
                obj["language_code"],
            ]


async def main():
    global api_token, languages, tor_browser_translations

    if len(sys.argv) != 2:
        print(f"Usage: {sys.argv[0]} API_KEY")
        print(
            "You can find your personal API key at: https://hosted.weblate.org/accounts/profile/#api"
        )
        return

    api_token = sys.argv[1]

    # Get the list of languages in one of the components
    # We use this component instead of the general list of languages because
    # we have some locales that are not in Tor Browser

    res = await api(f"/api/components/tor/tor-browser%252Ftb-aboutdialogdtd/translations/")
    for obj in res["results"]:
        await get_component_stats(obj)

    while res["next"] is not None:
        newpage = res["next"].split("/")[-1]
        res = await api(
            f"/api/components/tor/tor-browser%252Ftb-aboutdialogdtd/translations/{newpage}"
        )
        for newobj in res["results"]:
            await get_component_stats(newobj)

    # start building the page
    with open("stats/tb-stats.md", "w") as output_file:
        output_file.write("\n## Tor Browser translations\n\n")

    # Get the Tor Browser translations for each language and component
    tb_components = [
        name.strip()
        for name in resources_config["tor-browser"]["components"].split(", ")
    ]
    for component in tb_components:
        await get_tb_translation(component)

    print("")

    await docs_percent_output(90, 101)
    await docs_percent_output(50, 90)
    await docs_percent_output(0, 49)


if __name__ == "__main__":
    asyncio.run(main())
